import os
import requests
from bs4 import BeautifulSoup
from git import Repo, GitCommandError

# Clone the repository and navigate to it
repo_path = '/builds/snoeDev/ott'  # Update this path if necessary
repo = Repo(repo_path)

# Checkout the main branch
try:
    repo.git.checkout('main')
except GitCommandError as e:
    print(f"Failed to checkout branch: {e}")
    exit(1)

# Define scraping logic
url = "https://bapok.best"
headers = {
    'User-Agent': 'PlaylistFree: bapok.best'
}

response = requests.get(url, headers=headers)
soup = BeautifulSoup(response.text, 'html.parser')
content = soup.get_text()

# Check if the scraped content has at least 200 lines
lines = content.splitlines()  # Split content into lines
if len(lines) < 200:
    print(f"Scraped content has only {len(lines)} lines, which is less than the required 200 lines. Exiting script.")
    exit(0)  # Exit gracefully

# Write scraped content to playlist.m3u
file_path = os.path.join(repo_path, 'playlist.m3u')
with open(file_path, 'w') as file:
    file.write(content)

# Commit and push changes
repo.index.add([file_path])
repo.index.commit("Update scraped content")

# Set the remote URL to use the GitLab CI/CD token for authentication
ci_token = os.getenv('CI_PUSH_TOKEN')
if not ci_token:
    print("CI_PUSH_TOKEN environment variable is missing.")
    exit(1)

# Configure remote with access token
repo.remotes.origin.set_url(f"https://oauth2:{ci_token}@gitlab.com/snoeDev/ott.git")

# Push changes to the remote repository
try:
    repo.remotes.origin.push()
    print("Changes pushed to the repository.")
except GitCommandError as e:
    print(f"An error occurred during push: {e}")
